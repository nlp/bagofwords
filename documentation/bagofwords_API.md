---
sidebar_label: bagofwords.bagofwords
title: BagOfWords Class API
---

`BagOfWords` class, which counts the number of times a word appears in a catalog, the number of distinct documents where a word appears, and the cooccurence matrix of all unique words in a catalog.

## BagOfWords Objects

```python
class BagOfWords()
```

`BagOfWords` class, which counts the number of times a word appears in a catalog, the number of distinct documents where a word appears, and the cooccurence matrix of all unique words in a catalog.

A catalog is defined as a sequence of sequences of strings, where each string is supposed to be a token. This class counts the number of tokens. It is based on the class `sklearn.feature_extraction.text.CountVectorizer` ([documentation](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html)), which ouput some `scipy.sparse` sparse matrices ([documentation](https://docs.scipy.org/doc/scipy/reference/sparse.html)). Note that neither the `tokenizer` nor the `preprocessor` of this class is used, so the catalog can be passed in any way let free to the user.

 The size of each document is simply the size of the inner sequence, and this can be changed at will by the user. This preprocessing of the data, let to the user, consists in the main flexibility of the characteristics extracted by the `FeaturesCatalog` class. An example catalog is :

 ```python
 catalog = [['string11', 'string12', 'string13', ... 'string1N1'],
            ['string21', 'string22', ..., 'string2N2'],
            ...,
            ]
 ```
The size of each inner sequence determines the window for latter analysis, like `cooccurence` or `cobow_equivalence` or `pointwise_mutual_information`.

### \_\_init\_\_

```python
 | __init__(bow=list(), vocabularray=list(), verbose=True)
```

Takes as parameters :
 - `bow` : a `scipy.sparse` matrix, with D lines (representing the number of documents in the catalog) and W columns (representing the number of unique token in the catalog). Can be left empty for a new catalog, since the class methods will generate some new ones, and eventually fusion the already given counting with the new ones.
 - `vocabularray` : a list of unique tokens, in case it is not `None`, this list is given to `CountVectorizer`, which will only consider these tokens. The vocabulary can be later catched using the `vocabularray_` attribute of the class.
 - `verbose` : a boolean (default is `True`), to let the class methods print some message during the catalog parsing.

### \_check\_init

```python
 | _check_init()
```

In case both a bow and a vocabularray are given, check if they are compatible.

### \_\_repr\_\_

```python
 | __repr__()
```

Returns the sparse matrix representation.

### shape

```python
 | @property
 | shape()
```

Returns the sparse matrix shape.

### \_\_getitem\_\_

```python
 | __getitem__(n)
```

Returns the sparse matrix getter.

### nonzero

```python
 | nonzero()
```

Returns the sparse matrix indices of nonzero elements, as in scipy.sparse.

### \_check\_ov

```python
 | _check_ov(occurence, vocabulary)
```

Check whether the BOW and the vocabularray exist and are compatible.

### \_generate\_ov

```python
 | _generate_ov(catalog)
```

Generate the `occurence` and `vocabulary` object either from a given catalog or from the `self.catalog_` in case no `catalog` is given. Does not give any `tokenizer` or `preprocessor` to `CountVectorizer`.

### fit

```python
 | fit(catalog)
```

Generate the `bow_` and `vocabularray_` attributes.

### make\_boolean

```python
 | make_boolean(sparse, withdraw_diag=False)
```

Transform a sparse matrix into the same matrix, where all entries are replaced with one (for True) and zeros are let the same. If `withdraw_diag` (a Boolean) is True, then the diagonal is withdrawn. For later uses.

### count

```python
 | count(axis=0, unique=False)
```

Counts the number of times a unique tokens happens in a catalog, given as parameter.

If `axis=0` (by default), returns a `numpy.array` of shape (1,W) (`vocabularray_` size) with number of occuring token in the entire catalog.

If `axis=1`, returns a `numpy.array` of shape (1,D) (number of documents) with number of tokens per document.

If `unique=True`, replaces all entries in the BOW by 1 before calculating the sum. That is :
    - if `axis=0` (by default), returns the number of tokens observed, while counting one token per document at maximum
    - if `axis=1`, returns the number of unique tokens per document.

### entropy

```python
 | entropy(axis=0, normalize=True)
```

Calculate the [entropy](https://en.wikipedia.org/wiki/Entropy_(information_theory)) according to its definition in information theory, along a given `axis` of the BOW. This is just the sum over its axis elements times their natural logarithms. If `normalize=True` (value by default), then the logarithm in base N (with N the total number of tokens in the BOW) is used, and all elements of the BOW are divided by N as well. Then it becomes possible to compare the different entropies disregarding the size of the BOW.

### cooccur

```python
 | cooccur(axis=0)
```

Calculate the cooccurence matrix along the axis of the BOW matrix. Hereafter, a 'unique token' is a token appearing in the BOW, with its counting number equal to 1, that is, before any calculation, the entries of the BOW are resetted to 1.

If `axis=0`, returns the `cooccurence` matrix of size W x W, where W is the number of unique tokens in the catalog, with non-zeros entries counting the number of times two tokens appear in the same document.

If `axis=1`, returns the `cooccurence` matrix of size D x D, where D s the number of documents in the catalog, with non-zeros entries counting the number of times two documents have the same tokens in commom. This matrix can be seen as the adjacency matrix of the line graph of the cooccurence with `axis=0`, and vice-versa.

Note that the diagonal elements are not reseted to zero. That is, the diagonal elements `cooccurence[i,i]` counts
 - the number of unique tokens in the document i case `axis=1`, i.e. the size of the document i in term of unique token
 - the number of documents a unique token i appeared in, in case `axis=0`
These matrices can be seen as the Laplacian matrices of the graph and line graph, respectively, of the cooccurence matrices. That is, the degrees are the diagonal components, and the adjacency matrix corresponds to the non-diagonal elements.

### tfidf

```python
 | tfidf(**kwargs)
```

Calculate the TF-IDF according to [sklearn](https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.TfidfTransformer.html). Take all arguments of the sklearn class.

Returns a new `BagOfWords` instance with the TF-IDF matrix as `bow_`, and the previous `vocabularray_` attributes.

### cosine\_similarity

```python
 | cosine_similarity(axis=0)
```

Calculate the cosine similarity along the axis of the BOW matrix. Returns a symmetric sparse matrix whose entries correspond to non-null similarity along the given axis.

If the BOW matrix is a DxW sparse matrix, calculating the `cosine_similarity` along the `axis=0` returns a WxW matrix, and a DxD symmetric matrix in case `axis=1` is chosen.

### jaccard\_similarity

```python
 | jaccard_similarity(axis=0)
```

Calculate the Jaccard similarity along the axis of the BOW matrix. Returns a symmetric sparse matrix whose entries correspond to non-null similarity along the given axis. This Jaccard similarity is defined as on [Wikipedia/Jaccard_index](https://en.wikipedia.org/wiki/Jaccard_index) and [scipy.spatial.distance.jaccard](https://docs.scipy.org/doc/scipy/reference/generated/scipy.spatial.distance.jaccard.html), except it is faster since it use broadcasting of the entire sparse matrix.

Note the Jaccard similarity is defined when disregarding counting of the tokens in the BOW, i.e. all entries of the BOW are replaced with 1 before proceeding to calculation. In that sense, the Jaccard similarity is the [similarity of binary attributes](https://en.wikipedia.org/wiki/Jaccard_index#Similarity_of_asymmetric_binary_attributes). In particular, it means the Jaccard similarity calculated here is not exactly the [Jaccard score from sklearn.metrics.jaccard_score](https://scikit-learn.org/stable/modules/generated/sklearn.metrics.jaccard_score.html) as it would be calculated from the BOW, since the sklearn implementation uses [multi label criterion and average](https://scikit-learn.org/stable/modules/model_evaluation.html#average).

If the BOW matrix is a NxM sparse matrix, calculating the `jaccard_similarity` along the `axis=0` returns a MxM matrix, and a NxN symmetric matrix in case `axis=1` is chosen.

### graph\_equivalence

```python
 | graph_equivalence(adj_mat, make_boolean=True)
```

Calculate the number of disconnected graphs from an [adjacency matrix](https://en.wikipedia.org/wiki/Adjacency_matrix) `adj_mat`, supposed to represent an undirected graph. Returns a list of sets of indices, each set representing one class, and the indices being the indices of the matrix which belong to the class.

If `make_boolean` is True, the adjacency matrix is renderered boolean, i.e. a [distance matrix](https://en.wikipedia.org/wiki/Distance_matrix) is then transformed into a correct [adjacency matrix](https://en.wikipedia.org/wiki/Adjacency_matrix). In that case the diagonal elements are removed. Default is False.

### graph\_equivalence\_count

```python
 | graph_equivalence_count(adj_mat, make_boolean=True)
```

Calculate the number of disconnected graphs from an adjacency matrix. Returns the number of equivalence classes.

If one only needs the number of equivalence class, this method is faster than extracting `len(equivalences)` with `equivalence` the outcome of `graph_equivalence` method.

### LMI

```python
 | LMI(axis=0, normalize=True)
```

Alias for the `loose_mutual_information method.

### loose\_mutual\_information

```python
 | loose_mutual_information(axis=0, normalize=True)
```

Something like the method `pointwise_mutual_information` (MI), except it is done directly using the `cobow_` attribute of the object. That is, there is no notion of real bigrams ; entries are simply unordered set of words comprised in the size of a document in the catalog. This method gives a looze approximation of the (pointwise) MI, without the necessity to parse twice a catalog.

Returns a list of (increasing) order pseudo MI in the form

```python
[({'string1','string2'}, score1), ({'string1','string3'}, score2), ...]
```
with the `score` given by `score = log(alpha*cooccur[i,j]/(occur[i]*occur[j]))` with the coefficient `alpha = nb_occur**2/nb_cooccur`, with
 - `coccur[i,j]` the cooccurence between string `i` and `j`,
 - `occur[i]` the occurence of the string `i`
 - `nb_occur = sum(occur[i] over i)` is the total occurence of strings
 - `nb_cooccur = sum(cooccur[i,j] over i,j)` is the total number of cooccurences

### new\_tokens\_per\_document

```python
 | new_tokens_per_document(shuffle=False, fit=True)
```

Evaluate the number of new tokens discovered per opened document. Returns `new_words` a `numpy.array` of integers of size D (the number of documents in the catalog), representing the number of new documents found from the previous position to the actual one, that is, if `new_words[d] = 12`, it means that there is `12` more tokens in document `d` than there were in after document `d-1` was opened.

If `fit=True` (default to `True`), fit the standardized cumulative sum of `new_words` to a Beta distribution, with parameters `alpha` and `beta`. Otherwise both `alpha` and `beta` are `None`. This fit is based on the maximum likelihood estimates of the probability distribution of `new_words`, seen as the probability distribution function of a Beta distribution.

As the order of opening documents is a mater of probability, one can shuffle the catalog before extracting `new_words`, with `shuffle=True` (default is `False`, in which case the rows of the `bow_` matrix are parsed in their actual order).

Returns the tuple `new_words, alpha, beta`

### symmetric2list

```python
 | symmetric2list(sparse, axis=1)
```

Once a symmetric matrix is calculated somewhere else in this class, this method returns its non-zeros values in the form of an ordered list of tuples in the form:
```python
[({tok11,tok12},dist1),
 ({tok21,tok22},dist2),
 ]
```
with `tokx` some words in the `vocabularray_` (if `axis=1`, which is the value by default), or `tokx` some indices (if `axis=0`). Then the list is ordered in increasing values of `distx`, which is a float.

In fact this method just present the ordered version of the symmetric matrix in the form of an ordered list.

**Warnings :**
 - the returned list will never contain the diagonal elements of the sparse matrix, even if they exist in the original matrix.
 - there is anything to prevent from giving a non-symmetric matrix.

### filter\_bow0

```python
 | filter_bow0(conserved_indices, remove_empty=True)
```

Withdraw the columns of the BOW whose indices are not in the `conserved_indices`, and remove empty lines if `remove_empty=True`, to be integrated in later methods. Returns new `bow_` and `vocabularray_` attributes.

### filter\_bow1

```python
 | filter_bow1(conserved_indices, remove_empty=True)
```

Withdraw the lines of the BOW whose indices are not in the `conserved_indices`, and remove empty columns if `remove_empty=True`, to be integrated in later methods. Returns new `bow_` and `vocabularray_` attributes.

### withdraw\_documents

```python
 | withdraw_documents(indices, remove_empty=True)
```

Withdraw all the datas given by indices in `indices` along an axis, and remove empty the data along the other axis if `remove_empty` is `True`.

Returns a new `vocabularray_` and a new `bow_` attributes.

### keep\_documents

```python
 | keep_documents(indices, remove_empty=True)
```

Withdraw all the datas given by indices in `indices` along an axis, and remove empty the data along the other axis if `remove_empty` is `True`.

Returns a new

### withdraw\_tokens

```python
 | withdraw_tokens(token_sequence, remove_empty=True)
```

Withdraw all the tokens given as a `token_sequence` from the `FeaturesCatalog` object. Returns new `vocabularray_` and `bow_` objects, that can be given to a new `FeaturesCatalog` instance, and discarding the withdrawn tokens from the original BOW.

Take a `token_sequence` sequence of strings where the strings are tokens in the `vocabularray_` (will raise some `KeyError` in case on string is not in the `vocabularray_`).

Returns `new_vocab` and `new_occurence`, the filtered `vocabularray_` and `bow_` attributes. Does not change the attribute of the actual instance, but the outcome of this method can be fed to a new instance of the class `FeaturesCatalog`, as e.g.

```python
feats = FeaturesCatalog(catalog=some_catalog)
new_vocabularray, new_occurence = feats.withdraw_tokens([list of tokens])
new_feats = FeaturesCatalog(vocabularray=new_vocabularray,
                            occurence=new_occurence)
```
and such that most of the methods calculated by the `FeaturesCatalog` are again available on this restricted vocabulary (a notable exception is the `pointwise_mutual_information` method.

### keep\_tokens

```python
 | keep_tokens(token_sequence, remove_empty=True)
```

Keep all the tokens given as a `token_sequence` from the `FeaturesCatalog` object. Returns new `vocabularray_` and `bow_` objects, that can be given to a new `FeaturesCatalog` instance, and containing only the kept tokens from the original BOW.

Take a `token_sequence` sequence of strings where the strings are tokens in the `vocabularray_` (will raise some `KeyError` in case on string is not in the `vocabularray_`).

Returns `new_vocab` and `new_occurence`, the filtered `vocabularray_` and `bow_` attribute. Does not change the attribute of the actual instance, but the outcome of this method can be fed to a new instance of the class `FeaturesCatalog`, as e.g.

```python
feats = FeaturesCatalog(catalog=some_catalog)
new_vocabularray, new_occurence = feats.keep_tokens([list of tokens])
new_feats = FeaturesCatalog(vocabularray=new_vocabularray,
                            occurence=new_occurence)
```
and such that most of the methods calculated by the `FeaturesCatalog` are again available on this restricted vocabulary (a notable exception is the `partial_mutual_information` method.

### filter\_tokens\_length

```python
 | filter_tokens_length(length=3, remove_empty=True)
```

Withdraw all the tokens with length of token &lt;=`length` criterion in the `features` instance of the class `FeaturesCatalog`.

Returns `new_vocab` and `new_occurence` attributes with reduced `vocabularray_` and `bow_` attribute. Do not change the actual `BagOfWords` instance.

### filter\_documents\_length

```python
 | filter_documents_length(length=3, remove_empty=True)
```

Withdraw all the documents whose number of tokens &lt;=`length` criterion in the `features` instance of the class `FeaturesCatalog`.

Returns new `vocabularray_` and `bow_` attributes, that can be given to a new `FeaturesCatalog` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.

### filter\_tokens\_occurence

```python
 | filter_tokens_occurence(occurence=1, remove_empty=True)
```

Withdraw all the tokens with occurence&lt;=`occurence` criterion in the `features` instance of the class `FeaturesCatalog`. The tokens are counted only once per document.

Returns new `vocabularray_` and `bow_` attributes, that can be given to a new `FeaturesCatalog` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.

### filter\_documents\_equivalent

```python
 | filter_documents_equivalent()
```

Withdraw all the document being equivalent to an other one. Tow documents are considered equivalent when they present the same tokens, disregarding the number of occurence of each token in a document.

Returns new `vocabularray_` and `bow_` attributes, that can be given to a new `FeaturesCatalog` instance, and containing only the kept documents from the original BOW. Do not change the actual `BagOfWords` instance.

Consider `reconstruct_catalog` as well, which does something quite similar.

### reconstruct\_catalog

```python
 | reconstruct_catalog(withdraw_duplicates=True)
```

Reconstruct a factice catalog from the BOW. The order of the tokens in the initial documents is not conserved. Usefull once a BOW has been filtered.      

Returns a new catalog (list of lists of tokens) which can be fed to a `BagOfWords().fit(new catalog)`

### load

```python
 | load(file_name, allow_pickle=True)
```

Load the datas previously saved using the method `save`, take the same parameter as the `save` method (i.e. do not give the `_vocabulary.npy` or `_occurence.npz` extensions).

### save

```python
 | save(file_name)
```

Save both the `vocabulary_` and the `bow_` attributes in two separated files.
