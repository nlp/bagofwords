networkx==2.7
numpy>=1.21
scikit-learn>=0.20
scipy>=1.7
tqdm>=4.62
