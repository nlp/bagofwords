#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test of the BagOfWords class

Run as 
```bash
python3 -m unittest -v test/test_BagOfWords.py
```
from the main folder in order to get the module elements prior to its installation.
"""

import unittest as ut

import numpy as np

from iambagging.bagofwords import BagOfWords as BOW
from scripts.generate_random_catalog import test_catalog as catalog

class TestBagOfWords(ut.TestCase):
    
    def setUp(self):
        self.bow = BOW(verbose=False).fit(catalog)
        return None

    def test_exist(self):
        """bow_ and vocabularray_ existence, and their raising errors"""
        self.assertTrue(hasattr(self.bow,'bag'))
        self.assertTrue(hasattr(self.bow,'vocabularray'))
        self.assertTrue(hasattr(self.bow,'doctionarray'))
        self.assertIsNone(self.bow._check_init())
        with self.assertRaises(AttributeError):
            self.bow._check_ov(range(10),range(5))
            self.bow._check_ov(range(10),None)
            self.bow._check_ov(None,range(10))
        return None        
    
    def test_vocabularray(self):
        """vocabularray_ is of correct dimension, and contains all tokens in the catalog"""
        words_set = {w for c in catalog for w in c}
        unique_words = len(words_set)
        unique_doc = len(catalog)
        self.assertTrue(self.bow.bag.shape == (unique_doc,unique_words))
        word_in_vocabularray = [word in self.bow.vocabularray 
                                for word in words_set]
        self.assertTrue(all(word_in_vocabularray))
        with self.assertRaises(TypeError):
            self.bow.shape(2)
        # test the new_tokens_per_document method
        new_words, alpha, beta = self.bow.new_tokens_per_document(shuffle=False,fit=True)
        self.assertTrue(sum(new_words)==unique_words)
        return None
    
    def test_count(self):
        """.count method counts correctly the tokens, and .cooccur method is self-consistent with .count method"""
        count_dict1 = dict()
        for doc in catalog:
            for word in doc:
                try:
                    count_dict1[word] += 1
                except KeyError:
                    count_dict1[word] = 1
        count = self.bow.count(axis=0, unique=False)
        count_dict2 = {self.bow.vocabularray[i,]:count[0,i] 
                       for i in range(self.bow.bag.shape[1])}
        c1_equal_c2 = [count_dict1[k]==count_dict2[k] for k in count_dict1.keys()]
        self.assertTrue(all(c1_equal_c2))
        # count the unique occurence with the diagonal elements of cooccur method
        for i in [0,1]:
            count = self.bow.count(axis=i, unique=True)
            count = count.flatten()
            cooccur = self.bow.cooccur(axis=i)
            cooccur = cooccur.diagonal().flatten()
            mask = count==cooccur
            self.assertTrue(all(mask))
        return None
    
    def test_dimension(self):
        """all similarity matrices have correct dimensions"""
        words_set = {w for c in catalog for w in c}
        shapes = [len(words_set),len(catalog)]
        for method in ['entropy','count']:
            for axis in [0,1]:
                res = getattr(self.bow,method)(axis=axis)
                res = res.flatten()
                self.assertTrue(res.shape==(shapes[axis],))
        for method in ['cooccur','jaccard_similarity','cosine_similarity',
                       'loose_mutual_information',]:
            for axis in [0,1]:
                res = getattr(self.bow,method)(axis=axis)
                self.assertTrue(res.shape==(shapes[axis],shapes[axis]))
        self.assertTrue(self.bow.tfidf().shape==self.bow.shape)
        return None
    

class TestBagOfWordsSparseMatrixAttributes(ut.TestCase):
    
    def setUp(self):
        self.catalog = catalog
        self.bow = BOW(verbose=False)
        self.bow.fit(self.catalog)
        return None
    
    def test_shape(self):
        """Does the shape attribtue works correctly ?"""
        vocab = set(w for doc in catalog for w in doc)
        catal = len(catalog)
        self.assertIsInstance(self.bow.shape, tuple)
        self.assertEqual(self.bow.shape, self.bow.bag.shape)
        self.assertEqual(self.bow.shape, (catal, len(vocab)))
        return None
    
    def test_todense(self):
        """Does todense() works as expected ?"""
        self.assertIsInstance(self.bow.todense(), np.matrix)
        td1 = self.bow.bag.todense()
        td2 = self.bow.todense()
        self.assertEqual(td1.shape, td2.shape)
        for td1_, td2_ in zip(td1, td2):
            for td1__, td2__ in zip(td1_.tolist()[0], td2_.tolist()[0]):
                self.assertEqual(td1__, td2__)
        return None
    
    def test_nonzero(self):
        """Does nonzero() works as excepted ?"""
        self.assertIsInstance(self.bow.nonzero(), tuple)
        for nz in self.bow.nonzero():
            self.assertIsInstance(nz, np.ndarray)
        nz1 = self.bow.nonzero()
        nz2 = self.bow.bag.nonzero()
        self.assertEqual(len(nz1), len(nz2))
        for nz1_, nz2_ in zip(nz1, nz2):
            self.assertEqual(len(nz1_), len(nz2_))
            for nz1__, nz2__ in zip(nz1_, nz2_):
                self.assertEqual(nz1__, nz2__)
        return None        

class TestBagOfWordsGraph(ut.TestCase):
    
    def setUp(self):
        self.bow = BOW(verbose=False).fit(catalog)
        self.cooccur0 = self.bow.cooccur(axis=0)
        self.cooccur1 = self.bow.cooccur(axis=1)
        return None

    def test_dimension(self):
        """cooccur has the correct dimension"""
        words_set = {w for c in catalog for w in c}
        unique_words = len(words_set)
        unique_doc = len(catalog)
        self.assertTrue(self.cooccur0.shape==(unique_words,unique_words))
        self.assertTrue(self.cooccur1.shape==(unique_doc,unique_doc))
        return None
    
    def test_equivalence(self):
        """.graph_equivalence and .graph_equivalence_count are consistent with each other"""
        for b in [True,False]:
            equiv = self.bow.graph_equivalence(self.cooccur0,make_boolean=b)
            n = self.bow.graph_equivalence_count(self.cooccur0,make_boolean=b)
            self.assertTrue(len(equiv)==n)
        return None

class TestBagOfWordsFilter(ut.TestCase):
    
    def setUp(self):
        self.bow = BOW(verbose=False).fit(catalog)
        return None

    def test_dimension(self):
        """withdraw one document from the BOW, and check the corresponding dimensions"""
        words_set = list({w for c in catalog for w in c})
        unique_words = len(words_set)
        unique_doc = len(catalog)
        # withdraw some tokens
        bow, vocab, doc = self.bow.filter_cols(range(unique_words-1),remove_empty=False)
        self.assertTrue(vocab.shape[0]==self.bow.vocabularray.shape[0]-1)
        self.assertTrue(len(doc) == unique_doc)
        self.assertTrue(bow.shape[0]==self.bow.bag.shape[0])
        self.assertTrue(bow.shape[1]==self.bow.bag.shape[1]-1)
        self.assertTrue(sum(self.bow.vocabularray[:-1] == vocab) == len(vocab))
        # withdraw some documents
        bow, vocab, doc = self.bow.filter_rows(range(unique_doc-1),remove_empty=False)
        self.assertTrue(vocab.shape==self.bow.vocabularray.shape)
        self.assertTrue(sum(self.bow.vocabularray == vocab)==len(vocab))
        self.assertTrue(bow.shape[0]==self.bow.bag.shape[0]-1)
        self.assertTrue(bow.shape[1]==self.bow.bag.shape[1])
        self.assertTrue(sum(self.bow.doctionarray[:-1] == doc) == len(doc))
        return None
    
    def test_removeempty(self):
        """the filtering functions remove correctly the empty documents"""
        n = 12
        line0 = list(range(n)) # empty column with indices 0
        line1 = [0 for _ in range(n)] # empty line
        array = [line0,line1]
        with self.assertRaises(ValueError):
            BOW(bag=array,vocabularray=line0[:n-1],verbose=False)
        bow = BOW(bag=array,
                  vocabularray=line0,
                  doctionarray=range(len(array)),
                  verbose=False)
        # withdraw some tokens
        bow_, vocab, doc = bow.filter_cols(line0,remove_empty=True)
        self.assertTrue(vocab.shape[0]==bow.vocabularray.shape[0])
        self.assertTrue(bow_.shape[0]==bow.bag.shape[0]-1)
        self.assertTrue(bow_.shape[1]==bow.bag.shape[1])
        self.assertTrue(sum(bow.vocabularray == vocab) == len(vocab))
        self.assertTrue(sum(bow.doctionarray[:-1] == doc) == len(doc))
        # withdraw empty columns (the 0-th one)
        bow_, vocab, doc = bow.filter_rows(range(2),remove_empty=True)
        self.assertTrue(vocab.shape[0]==bow.vocabularray.shape[0]-1)
        self.assertTrue(bow_.shape[0]==bow.bag.shape[0])
        self.assertTrue(bow_.shape[1]==bow.bag.shape[1]-1)
        self.assertTrue(sum(bow.vocabularray[1:] == vocab) == len(vocab))
        self.assertTrue(sum(bow.doctionarray == doc) == len(doc))
        return None
        
if __name__ == '__main__':
    ut.main()


