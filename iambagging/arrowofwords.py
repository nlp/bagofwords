#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Class ArrowOfWords


"""
from typing import List, Tuple
from collections import defaultdict, Counter

import numpy as np
import scipy.sparse as sp

from .directedbag import DirectedBag

class ArrowOfWords(DirectedBag):
    
    def _generate_bag(self, catalog,):
        """
Create sparse AOW matrix, vocabulary and document-indexer
Inspired from sklearn implementation of the CountVectorizer, with some functionnalities dismissed, see the source code from 
https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html
        """
        vocabulary, slices = defaultdict(), []
        def _default_factory(): return len(vocabulary)+1
        vocabulary.default_factory = _default_factory
        doctio, docid = list(), None
        
        j_indices, indptr, values = list(), list(), list()
        indptr.append(0)
        start_slice, stop_slice = 0, 0
        for docid, doc in self._tqdm(enumerate(catalog)):
            if self.docextractor:
                docid, doc = self.docextractor(doc)
            if len(doc) == 0:
                # otherwise indptr, slices and doctio are updated
                continue
            token_position = {}
            # each document starts with a 1 in the first column
            for token in doc[:1]:
                token_position[0] = 1
            for pos,token in enumerate(doc):
                token_idx = vocabulary[token]
                if token_idx not in token_position:
                    token_position[token_idx] = pos+1
                else:
                    # if repeated indice, write a line and start over
                    j_indices.extend(token_position.keys())
                    values.extend(token_position.values())
                    indptr.append(len(j_indices))
                    token_position = {}
                    stop_slice += 1
                    token_position[token_idx] = pos+1
            j_indices.extend(token_position.keys())
            values.extend(token_position.values())
            indptr.append(len(j_indices))
            slices.append(slice(start_slice, stop_slice+1))
            start_slice, stop_slice = stop_slice+1, stop_slice+1
            doctio.append(str(docid))

        # disable defaultdict behaviour
        vocabulary = dict(vocabulary)
        # shift the vocabulary by one position
        inverted_vocab = {v-1:k for k,v in vocabulary.items()}
        vocab = [inverted_vocab[i] for i in range(len(inverted_vocab))]
        vocabularray = np.array(vocab, dtype=str)
        
        doctionarray = np.array(doctio, dtype=str)

        j_indices = np.asarray(j_indices, dtype=int)
        indptr = np.asarray(indptr, dtype=int)
        values = np.asarray(values, dtype=int)

        aow = sp.csr_matrix((values,j_indices,indptr),
                            shape=(len(indptr)-1, len(vocabulary)+1),
                            dtype=int)
        aow.sort_indices()
        
        self._slices = slices
        
        return aow, vocabularray, doctionarray
    
    def _reconstruct_document_from_slice(self, slice_: slice) -> List[str]:
        """Reconstruct a document given by a slice on the NoW matrix."""
        # take the sub-matrix
        docsparse = self.bag[slice_, 1:]
        # keep only the positions of the non-zero values
        indices = docsparse.nonzero()
        if len(indices[0]) == 0:
            return list()
        positions = np.array(docsparse[indices])
        positions = positions.reshape((len(indices[0]),))
        # sort the positions to get the indices
        args_sorted = np.argsort(positions)
        # convert to tokens
        document = [self.vocabularray[indices[1][a]] for a in args_sorted]
        return document
    
    def _docsparse_to_countidx(self, docsparse):
        """Construct the counter necessary for converting to BOW"""
        # take indices of non-zero values of the sparse matrix representing 
        # the document, 
        indices = docsparse.nonzero()
        # and counts the associated indices, shifted by one since first column
        # is not-representating of the vocabulary
        return dict(Counter(indices[1]-1))

    def _token_position_to_document_list(self, position):
        """Get all the document_ids associated to a token position"""
        # shift the position by one due to the first column of the bag used
        # for indexing starting documents
        line_numbers = self.bag[:, position+1].nonzero()[0]
        docids = self._line_numbers_to_docids(line_numbers)
        return docids 
    
    def construct_doc_boundaries(self,):
        """Withdraw the first colum that represents the document boundaries, and returns the data, vocabularray columns indices and the document boundaries that correspond to this reduced matrix."""
        # withdraw the first column that represents the document boundaries
        idx, idy = self.nonzero() # capture the columns elements, idx is useless
        mask = idy == 0 
        data, vocols = self.data[~mask], idy[~mask]
        datids2docids = self.datids2docids[~mask]
        # recalculate the boundatids for this reduced matrix
        doc_boundaries = np.flatnonzero(np.diff(datids2docids))+1
        # add the 0 position and the final position
        doc_boundaries = np.concatenate([np.array([0,]), 
                                         doc_boundaries, 
                                         np.array([len(data),])]) 
        return data, vocols, doc_boundaries
    
    def construct_sortokids(self,):
        """Construct the succesive token indices in the order of the catalog and document, from the first token of the first document to the last token of the last document. Documents are not splitted, this method returns a one-dimensional arrays of size given by the total number of tokens. Use `boundatids` and/or `datids2docids` to construct the boundaries of the documents."""
        data, vocols, doc_boundaries = self.construct_doc_boundaries()
        sorted_colidx = np.full(doc_boundaries[-1], 0)
        argshift = 0
        for start, stop in self._tqdm(zip(doc_boundaries[:-1], 
                                          doc_boundaries[1:])):
            sorted_colidx[start:stop] = np.argsort(data[start:stop]) + argshift
            argshift += stop - start 
        self._sortokids = vocols[sorted_colidx] -1 # shift to fit the vocabularray idx
        return None
    
    def construct_toknextok(self, nb_nextoks=1):
        """Construct all successive tokenids in raws of a table with size of raw is given by nb_nextoks + 1"""
        # construct all shifted tokenids
        toksnextoks = [self.sortokids[i:len(self.sortokids)-nb_nextoks+i] 
                       for i in range(nb_nextoks+1)]
        toknextok = np.vstack(toksnextoks).T
        # withdraw lines that correspond to boundaries of documents
        # in order to withdraw the corresponding lines in toknextok 
        # since they do not correspond to neighbors in the original documents
        data, vocols, doc_boundaries = self.construct_doc_boundaries()
        # construct a table of [start, stop] position in the document representation
        doc_bounds = np.vstack([doc_boundaries[:-1], 
                                doc_boundaries[1:] - nb_nextoks]).T
        # withdraw document that are too short for the toknextok to exist
        doc_mask = np.diff(doc_boundaries) > nb_nextoks
        doc_bounds = doc_bounds[doc_mask]
        # mask of withdrawn toknextoks
        mask = np.full(len(data)-nb_nextoks, False, dtype=bool)
        for start, stop in self._tqdm(doc_bounds):
            mask[start:stop] = np.full(stop-start, True)
        toknextok = toknextok[mask]
        return toknextok 

    def filter_rows(self, conserved_indices, remove_empty=True):
        withdrawn_indices = set(range(self.bag.shape[0])).difference(conserved_indices)
        # since constructing the mask is costy, find the shortest loop
        if len(withdrawn_indices) > len(conserved_indices):
            mask = np.full(self.bag.shape[0], False)
            for docid in conserved_indices:
                mask += self.linids2docids == docid
        else:
            mask = np.full(self.bag.shape[0], True)
            for docid in withdrawn_indices:
                mask *= self.linids2docids != docid
        new_bag = self.bag[mask, :]
        new_vocab = self.vocabularray
        new_doctio = self.doctionarray[tuple(conserved_indices),]
        return new_bag, new_vocab, new_doctio
    
    def filter_tok(self, conserved_indices, remove_empty=True):
        raise NotImplementedError()
        
        