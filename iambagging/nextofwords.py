#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Class NextOfWords

"""
from typing import List, Tuple
from collections import defaultdict, Counter
# import array
import numpy as np
import scipy.sparse as sp

from .directedbag import DirectedBag

class NextOfWords(DirectedBag):
    
    def _generate_bag(self, catalog,):
        """
Create sparse NOW matrix (NextOfWords matrix), and vocabulary
Inspired from sklearn implementation of the CountVectorizer, with some functionnalities dismissed, see the source code from 
https://scikit-learn.org/stable/modules/generated/sklearn.feature_extraction.text.CountVectorizer.html
        """
        vocabulary, slices = defaultdict(), []
        # initialize the vocabulary with id=1
        def _default_factory(): return len(vocabulary)+1
        vocabulary.default_factory = _default_factory
        doctio, docid = list(), None
        
        j_indices, indptr, values = list(), list(), list()
        indptr.append(0)
        start_slice, stop_slice = 0, 0
        for docid, doc in self._tqdm(enumerate(catalog)):
            if self.docextractor:
                docid, doc = self.docextractor(doc)
            token_position = {}
            if len(doc) == 0:
                # otherwise indptr, slices and doctio are updated
                continue
            # initialize the first column with the first tokenId
            for tok in doc[:1]: 
                token_position[0] = vocabulary[tok]
            for tok1, tok2 in zip(doc[:-1],doc[1:]):
                token_id1 = vocabulary[tok1]
                token_id2 = vocabulary[tok2]
                if token_id1 not in token_position:
                    token_position[token_id1] = token_id2
                else:
                    # if repeated indice, write a line and start over
                    # this line will have empty (= 0) first column
                    j_indices.extend(token_position.keys())
                    values.extend(token_position.values())
                    indptr.append(len(j_indices))
                    token_position = {}
                    stop_slice += 1
                    token_position[token_id1] = token_id2
            j_indices.extend(token_position.keys())
            values.extend(token_position.values())
            indptr.append(len(j_indices))
            slices.append(slice(start_slice, stop_slice+1))
            start_slice, stop_slice = stop_slice+1, stop_slice+1
            doctio.append(str(docid))

        # disable defaultdict behaviour
        vocabulary = dict(vocabulary)
        # shift the vocabulary by one position
        inverted_vocab = {v-1:k for k,v in vocabulary.items()}
        vocab = [inverted_vocab[i] for i in range(len(inverted_vocab))]
        vocabularray = np.array(vocab, dtype=str)
        
        doctionarray = np.array(doctio, dtype=str)

        j_indices = np.asarray(j_indices, dtype=int)
        indptr = np.asarray(indptr, dtype=int)
        values = np.asarray(values, dtype=int)

        now = sp.csr_matrix((values,j_indices,indptr),
                            shape=(len(indptr)-1, len(vocabulary)+1),
                            dtype=int)
        now.sort_indices()
        
        self._slices = slices
        
        return now, vocabularray, doctionarray
    
    def _reconstruct_document_from_slice(self, slice_: slice) -> List[str]:
        """Reconstruct a document given by a slice on the NoW matrix."""
        document = list()
        # take the sub-matrix
        docsparse = self.bag[slice_, :]
        # take the first token
        next_tokid = docsparse[0, 0]
        document.append(self.vocabularray[next_tokid-1])
        # total number of tokens to be captured in the document
        nb_tot_tokid = docsparse.count_nonzero()
        # token already seen in the doc, flag to pass to the next line
        seen_tokid = [next_tokid,]
        # how much token have been already seen ? flag to stop the loop
        nb_seen_tokid = 1
        # line index in the docsparse
        line_idx = 0
        while nb_seen_tokid < nb_tot_tokid:
            next_tokid = docsparse[line_idx, next_tokid]
            document.append(self.vocabularray[next_tokid-1])
            nb_seen_tokid += 1
            # if the next token is not already in the flag to pass to next line
            if next_tokid not in seen_tokid:
                seen_tokid.append(next_tokid)
                # else: pass to the next line, and start over with fresh tokens
            else:
                seen_tokid = [next_tokid,]
                line_idx += 1
        return document
    
    def _docsparse_to_countidx(self, docsparse):
        """Method to reconstruct the BOW in the parent class"""
        return dict(Counter(docsparse.data-1))
    
    def _token_position_to_document_list(self, position):
        """Get all the document_ids associated to a token position"""
        # shift the position in vocabularray by one due to the first column
        mask_sparse = self.bag.data == position + 1
        line_numbers = self.nonzero()[0][mask_sparse]
        docids = self._line_numbers_to_docids(line_numbers)
        return docids

    def construct_toknextok(self, nb_nextoks=1):
        """
A naive way of counting all nextoken of a given token is to calculate
```python
adj_dict = {i-1: dict(Counter(self.bag[:,i].data-1)) 
            for i in range(1,self.bag.shape[1])}
```
So we use a better approach, that is, one generates the list of `[token_idx, nextoken_idx]` and we count the associated tuples. It returns a dictionary of tuples that counts the tuples: 
```python
{(token_id1, nextoken_id1): count1, 
 (token_id2, nextoken_id2): count2, ...}
```
        """
        if nb_nextoks > 1:
            mess = "Cannot calculate for more than one nextok."
            mess += " Use ArrowOfWords object if required."
            raise ValueError(mess)
        rows, cols = self.bag.nonzero()
        mask = cols != 0 # withdraw first column since we want (tokid, nextokid)
        # and first column contains no nextokid
        cols = cols[mask] - 1 # shift to get the vocabularray index
        datas = self.bag.data[mask] - 1 # shift to get the vocabularray index
        # construct a table with row = [token_idx, nextoken_idx]
        tuples = np.vstack([cols, datas]).T
        return tuples
    
    def filter_rows(self, conserved_indices, remove_empty=True):
        withdrawn_indices = set(range(self.bag.shape[0])).difference(conserved_indices)
        # since constructing the mask is costy, find the shortest loop
        if len(withdrawn_indices) > len(conserved_indices):
            mask = np.full(self.bag.shape[0], False)
            for docid in conserved_indices:
                mask += self.linids2docids == docid
        else:
            mask = np.full(self.bag.shape[0], True)
            for docid in withdrawn_indices:
                mask *= self.linids2docids != docid
        new_bag = self.bag[mask, :]
        new_vocab = self.vocabularray
        new_doctio = self.doctionarray[tuple(conserved_indices),]
        return new_bag, new_vocab, new_doctio
    
    def filter_tok(self, conserved_indices, remove_empty=True):
        mess = "To reconstruct an entire catalog with filtered tokens is faster than calculating this."
        raise NotImplementedError(mess)
        # withdrawn_indices = set(range(self.bag.shape[1])).difference(conserved_indices)
        # # in case the withdrawn tokens are in the first column, 
        # def construct_mask_firstcol(new_bag, withdrawn_indices, conserved_indices):
        #     first_col = new_bag[:,0].toarray().flatten()
        #     # since constructing the mask is costy, find the shortest loop
        #     # capture all the rows containing the conserved indices in first column
        #     if len(withdrawn_indices) > len(conserved_indices):
        #         mask_firstcol = np.full(new_bag.shape[0], True)
        #         for vocid in conserved_indices:
        #             mask_firstcol *= first_col != vocid
        #     else:
        #         mask_firstcol = np.full(new_bag.shape[0], False)
        #         for vocid in withdrawn_indices:
        #             mask_firstcol += first_col == vocid
        #     return mask_firstcol
        # # capture all the next tokens per document
        # new_bag = self.bag.copy()
        # mask_firstcol = construct_mask_firstcol(new_bag, 
        #                                         withdrawn_indices,
        #                                         conserved_indices)
        # flag = np.sum(mask_firstcol)
        # while flag:
        #     # capture the nexts tokens
        #     nexts = new_bag[mask_firstcol, 0].toarray().flatten()
        #     nexts = new_bag[mask_firstcol, nexts].reshape(len(nexts))
        #     # rewrite the nexts tokens in the first column
        #     new_bag[mask_firstcol, 0] = nexts
        #     # start again in case we put non-desired token in the first row
        #     mask_firstcol = construct_mask_firstcol(new_bag, 
        #                                             withdrawn_indices,
        #                                             conserved_indices)
        #     flag = np.sum(mask_firstcol)
        
        # # now shift the remaining tokens that are upper to the withdrawn tokens
        # data = new_bag.data
        # # since constructing the mask is costy, find the shortest loop
        # # capture all the positions correspondig to the kept tokens
        # if len(withdrawn_indices) > len(conserved_indices):
        #     mask = np.full(len(data), False)
        #     for vocid in conserved_indices:
        #         mask += data == vocid
        # else:
        #     mask = np.full(len(data), True)
        #     for vocid in withdrawn_indices:
        #         mask *= data != vocid
        # idx, idy = new_bag.nonzero()
        # # keep only interesting tokens
        # idx, idy = idx[mask], idy[mask]
        # data = data[mask]
        # # and finally shift everything
        # if len(withdrawn_indices) > len(conserved_indices):
        #     shift = 1
        #     for vocid in sorted(conserved_indices):
        #         mask = data == vocid
        #         data[mask] -= vocid - shift
        #         idy[mask] -= vocid - shift
        #         shift += 1
        # else:
        #     for vocid in withdrawn_indices:
        #         mask = data > vocid
        #         data[mask] -= 1
        #         idy[mask] -= 1
        # new_bag = sp.csr_matrix((data, (idx,idy)), 
        #                         shape=(self.bag.shape[0],len(conserved_indices)),
        #                         dtype=int)
        # new_vocab = self.vocabularray[tuple(conserved_indices),]
        # new_doctio = self.doctionarray
        # return new_bag, new_vocab, new_doctio
        
        
