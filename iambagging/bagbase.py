#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Base class for the bag-of-words like extractors.
"""

from datetime import datetime as dt
from typing import Iterable, Tuple, List

from tqdm import tqdm 
import numpy as np
import scipy.sparse as sp

class BagBase():
    """
`BagBase` class is the base class for all the bag-of-words in the rest of the library.

A `docextractor` is a function that takes a document and returns a tuple `docid, doc` with `doc` an iterable. Those are supposed to be a string and a list of strings. In practice, `docextractor` allows great versatility, since e.g. it can take the extract of a DB, and return the primary key of the document (the `docid` in the form of a string) and the tokens that can be constructed in the flow (the `doc` itself).

    """
    
    def __init__(self,
                 bag=sp.dok_matrix((0,0), dtype=int), 
                 vocabularray=list(),
                 doctionarray=list(),
                 docextractor=None,
                 verbose=True,
                 **kwargs):
        """
Takes as parameters : 
 - `bag` : a `scipy.sparse` matrix, with D lines (representing the number of documents in the catalog) and W columns (representing the number of unique token in the catalog). Can be left empty for a new catalog, since the class methods will generate some new ones, and eventually fusion the already given counting with the new ones.
 - `vocabularray` : a list of unique tokens, in case it is not `None`, this list is given to `CountVectorizer`, which will only consider these tokens. The vocabulary can be later catched using the `vocabularray_` attribute of the class.
 - `verbose` : a boolean (default is `True`), to let the class methods print some message during the catalog parsing.
        """
        if isinstance(bag, BagBase):
            kwargs = {k:bag.__dict__[k]
                      for k in ['bag', 'vocabularray', 'doctionarray', 
                                'verbose', 'docextractor']}
        else:
            kwargs = {'bag': bag, 
                      'vocabularray': vocabularray,
                      'doctionarray': doctionarray,
                      'docextractor': docextractor,
                      'verbose': verbose}
        self._init_(**kwargs)
        return None
    
    def _init_(self, **kwargs):
        self.bag = sp.csr_matrix(kwargs['bag'], dtype=int)
        self.vocabularray = np.array(kwargs['vocabularray'], dtype=str)
        self.doctionarray = np.array(kwargs['doctionarray'], dtype=str)
        self.docextractor = kwargs['docextractor']
        self._check_init()
        self.verbose = bool(kwargs['verbose'])
        return None        
    
    def _check_init(self,):
        """In case a bag, a vocabularray and a doctionarray are given, check if they are compatible. Raise a ValueError if they are not."""
        m = "bag of words (bag) and vocabularray and doctionarray must be compatile"
        cond1 = self.shape[1] != self.vocabularray.shape[0]
        cond2 = self.shape[0] != self.doctionarray.shape[0]        
        vocabularray_ = np.prod(self.vocabularray.shape) != 0
        doctionarray_ = np.prod(self.doctionarray.shape) != 0
        bag_exists = np.prod(self.shape) != 0
        if bag_exists:
            if (vocabularray_ and cond1) or (doctionarray_ and cond2):
                raise ValueError(m)
            if vocabularray_ and doctionarray_:
                if cond1 or cond2:
                    raise ValueError(m)
        return None

    def _tqdm(self, generator) -> Iterable:
        """Returns a tqdm generator if verbose is True else returns the bare generator."""
        return tqdm(generator) if self.verbose else generator    
    
    def __repr__(self,):
        """Returns the sparse matrix representation."""
        return self.bag.__repr__()
    
    @property
    def shape(self,) -> Tuple[int, int]:
        """Returns the sparse matrix shape."""
        return NotImplementedError
    
    def todense(self,):
        """Returns the dense version of the bag."""
        return self.bag.todense()
    
    def nonzero(self,):
        """Returns the sparse matrix indices of nonzero elements, as in scipy.sparse."""
        return self.bag.nonzero()
    
    @property
    def data(self,):
        """Returns the array of non-zero datas."""
        return self.bag.data
    
    def __getitem__(self,n):
        """Returns the sparse matrix getter."""
        return self.bag.__getitem__(n)
    
    def _generate_bag(self, catalog: Iterable[List[str]],
                      ) -> Tuple[sp.csr_matrix, np.array, np.array]:
        """ Generate the `bag`, `vocabularray` and `doctionarray` objects from a given catalog.
        """
        return NotImplementedError
    
    def fit(self, catalog: Iterable[List[str]]):
        """Generate the `bag`, the `vocabularray` and the `doctionarray` attributes."""
        t0 = dt.now()
        if self.verbose:
            print("{}, start extraction".format(t0.strftime("%Y-%m-%d %H:%M:%S")))
        self.bag, self.vocabularray, self.doctionarray = self._generate_bag(catalog)
        if self.verbose:
            t = dt.now()
            delta = t-t0
            m = "{}, {:.4} s".format(t.strftime("%Y-%m-%d %H:%M:%S"),
                                     delta.total_seconds())
            m += " to generate bag, vocabularray and doctionarray"
            print(m)
        return self

    def fit_transform(self, catalog):
        """Alias for `fit` method."""
        return self.fit(catalog)
    
    def docs_for_token(self, token):
        """Returns all the documents containing a given token"""
        t0 = dt.now()
        mask = self.vocabularray == str(token)
        position = np.flatnonzero(mask)
        if len(position) > 1:
            raise ValueError("token found several times in the vocabularray")
        docs = self._token_position_to_document_list(position[0])
        if self.verbose:
            t = dt.now()
            delta = t - t0
            m = "{}, {:.4} s".format(t.strftime("%Y-%m-%d %H:%M:%S"),
                                     delta.total_seconds())
            m += " to get {:,} documents".format(len(docs))
            m += " associated to token '{}'".format(token)
            print(m)
        return docs
    
    def _token_position_to_document_list(self, position):
        """Convert the position of a token in vocabularray to a list of documents"""
        return NotImplementedError

    @property 
    def vocount(self,): 
        """Total frequency of each token in the vocabularray. Returns a numpy array of length V, the size of the vocabularray."""
        if not hasattr(self, "_vocount"):
            self._vocount = self.construct_vocount()
        return self._vocount 
    
    def construct_vocount(self,):
        """Construct the vocount. To be implemented in children class."""
        raise NotImplementedError
    
    @property
    def docount(self,):
        """Number of tokens per document in the doctionarray. Returns a numpy array of length D, the size of the (non-empty document) catalog."""
        if not hasattr(self, "_docount"):
            self._docount = self.construct_docount()
        return self._docount        
    
    def construct_docount(self,):
        """Construct the docount. To be implemented in children class."""
        raise NotImplementedError
    
    def filter_cols(self, 
                    conserved_indices: Iterable[int],
                    remove_empty: bool=True) -> Tuple[sp.csr_matrix, np.array, np.array]:
        """Withdraw the columns of the bag whose indices are not in the `conserved_indices`, and remove empty lines if `remove_empty=True`, to be integrated in later methods. Returns new `bag`, `vocabularray` and `doctionarray` attributes."""
        raise NotImplementedError 
    
    def filter_rows(self,
                    conserved_indices: Iterable[int],
                    remove_empty: bool=True) -> Tuple[sp.csr_matrix, np.array, np.array]:
        """Withdraw the lines of the BOW whose indices are not in the `conserved_indices`, and remove empty columns if `remove_empty=True`, to be integrated in later methods. Returns new `bag`, `vocabularray` and `doctionarray` attributes."""
        raise NotImplementedError 

    def withdraw_documents(self, document_sequence, remove_empty=True):
        """
Withdraw all the documents given by the `document_sequence`, and remove the empty columns (tokens) of the bag if `remove_empty` is `True`.

Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new bag instance. Do not change the actual bag instance.
        """
        t0 = dt.now()
        self._check_init()
        mask = np.full(self.shape[0], False)
        for doc in document_sequence:
            mask += self.doctionarray == doc
        spurious_indices = np.flatnonzero(mask)
        indices = np.arange(self.shape[0])
        conserved_indices = set(indices).difference(set(spurious_indices))
        new_bag, new_vocab, new_doctio = self.filter_rows(conserved_indices,
                                                          remove_empty)
        if self.verbose:
            mess = "{:.4} s to filter the documents".format(dt.now()-t0)
            print(mess)
        return new_bag, new_vocab, new_doctio

    def keep_documents(self, document_sequence, remove_empty=True):
        """
Keep all the documents given by the `document_sequence`, and remove the empty columns (tokens) of the bag if `remove_empty` is `True`.

Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new `BagOfWords` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.
        """
        t0 = dt.now()
        self._check_init()
        mask = np.full(self.shape[0], False)
        for doc in document_sequence:
            mask += self.doctionarray == doc
        conserved_indices = np.flatnonzero(mask)
        new_bag, new_vocab, new_doctio = self.filter_rows(conserved_indices,
                                                          remove_empty)
        if self.verbose:
            mess = "{:.4} s to filter the documents".format(dt.now()-t0)
            print(mess)
        return new_bag, new_vocab, new_doctio
    
    def withdraw_tokens(self, token_sequence, remove_empty=True):
        """
Withdraw all the tokens given as a `token_sequence` from the `FeaturesCatalog` object. Returns new `vocabularray_` and `bow_` objects, that can be given to a new `FeaturesCatalog` instance, and discarding the withdrawn tokens from the original BOW.

Take a `token_sequence` sequence of strings where the strings are tokens in the `vocabularray_` (will raise some `KeyError` in case on string is not in the `vocabularray_`).

Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new `BagOfWords` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.
        """
        t0 = dt.now()
        self._check_init()
        mask = np.full(self.shape[1], False)
        for token in token_sequence:
            mask += self.vocabularray == token
        spurious_indices = np.flatnonzero(mask)
        indices = np.arange(self.shape[1])
        conserved_indices = set(indices).difference(spurious_indices)
        new_bag, new_vocab, new_doctio = self.filter_cols(conserved_indices,
                                                          remove_empty)
        if self.verbose:
            mess = "{:.4} s to filter the vocabulary".format(dt.now()-t0)
            print(mess)
        return new_bag, new_vocab, new_doctio

    def keep_tokens(self, token_sequence, remove_empty=True):
        """
Keep all the tokens given as a `token_sequence` from the `FeaturesCatalog` object. Returns new `vocabularray_` and `bow_` objects, that can be given to a new `FeaturesCatalog` instance, and containing only the kept tokens from the original BOW.

Take a `token_sequence` sequence of strings where the strings are tokens in the `vocabularray_` (will raise some `KeyError` in case on string is not in the `vocabularray_`).

Returns `new_vocab` and `new_occurence`, the filtered `vocabularray_` and `bow_` attribute. Does not change the attribute of the actual instance, but the outcome of this method can be fed to a new instance of the class `FeaturesCatalog`, as e.g.

```python
feats = FeaturesCatalog(catalog=some_catalog)
new_vocabularray, new_occurence = feats.keep_tokens([list of tokens])
new_feats = FeaturesCatalog(vocabularray=new_vocabularray,
                            occurence=new_occurence)
```
and such that most of the methods calculated by the `FeaturesCatalog` are again available on this restricted vocabulary (a notable exception is the `partial_mutual_information` method.
        """
        t0 = dt.now()
        self._check_init()
        mask = np.full(self.shape[1], False)
        for token in token_sequence:
            mask += self.vocabularray == token
        conserved_indices = np.flatnonzero(mask)
        new_bag, new_vocab, new_doctio = self.filter_cols(conserved_indices,
                                                          remove_empty)
        if self.verbose:
            mess = "{:.4} s to filter the vocabulary".format(dt.now()-t0)
            print(mess)
        return new_bag, new_vocab, new_doctio
    
    def filter_tokens_length(self, 
                             length=3, 
                             remove_empty=True,
                             order_relation='__le__'):
        """
Withdraw all the tokens with length of token `order_relation(length)` criterion.

Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new `BagOfWords` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.
        """
        lengths = np.array([len(w) for w in self.vocabularray[:,]])
        mask = getattr(lengths, order_relation)(int(length))
        if self.verbose:
            print("{:,} tokens withdrawn".format(np.sum(mask)))
        spurious_indices = np.flatnonzero(mask)
        indices = np.arange(self.shape[1])
        conserved_indices = set(indices).difference(spurious_indices)
        bag, vocab, doctio = self.filter_cols(conserved_indices,
                                              remove_empty=remove_empty)
        return bag, vocab, doctio

    def filter_documents_length(self, 
                                length=3, 
                                remove_empty=True,
                                order_relation='__le__'):
        """
Withdraw all the documents whose number of tokens `order_relation(length)` criterion.

Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new `BagOfWords` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.
        """
        count = self.docount
        mask = getattr(count.flatten(), order_relation)(int(length))
        if self.verbose:
            print("{:,} documents withdrawn".format(np.sum(mask)))
        spurious_indices = np.flatnonzero(mask)
        indices = np.arange(self.shape[0])
        conserved_indices = set(indices).difference(spurious_indices)
        bag, vocab, doctio = self.filter_rows(conserved_indices,
                                              remove_empty=remove_empty)
        return bag, vocab, doctio

    def filter_tokens_occurence(self, 
                                occurence=1, 
                                remove_empty=True,
                                order_relation='__le__'):
        """
Withdraw all the tokens with count.order_relation(`occurence`) criterion.
    
Returns new `bag`, `vocabularray` and `doctionarray` attributes, that can be given to a new `BagOfWords` instance, and containing only the kept tokens from the original BOW. Do not change the actual `BagOfWords` instance.
        """
        count = self.vocount
        mask = getattr(count.flatten(), order_relation)(int(occurence))
        if self.verbose:
            print("{:,} tokens withdrawn".format(np.sum(mask)))
        spurious_indices = np.flatnonzero(mask)
        indices = np.arange(self.shape[1])
        conserved_indices = set(indices).difference(spurious_indices)
        bag, vocab, doctio = self.filter_cols(conserved_indices,
                                              remove_empty=remove_empty)
        return bag, vocab, doctio        

    def load(self, file_name, allow_pickle=True):
        """Load the datas previously saved using the method `save`, take the same parameter as the `save` method (i.e. do not give the `_vocabularray.npy` or `_bagofwords.npz` (name depends on the class) or `_doctionarray.npy` extensions)."""
        class_name = self.__class__.__name__
        file_root = file_name + '_' +class_name
        self.vocabularray = np.load(file_root + '_vocabularray.npy',
                                     allow_pickle=allow_pickle)
        self.doctionarray = np.load(file_root + '_doctionarray.npy',
                                    allow_pickle=allow_pickle)
        self.bag = sp.load_npz(file_root + '.npz')
        if self.verbose:
            t0 = dt.now()
            mess = "{}, Load ".format(t0.strftime("%Y-%m-%d %H:%M:%S"))
            mess += class_name + " OK"
            print(mess)
        return self
    
    def save(self, file_name, add_time_slug=True, add_class_name=True):
        """Save the `vocabularray`, the `doctionarray` and the `bag` attributes in two separated files."""
        
        time_slug = dt.now().strftime("%Y%m%d")
        class_name = self.__class__.__name__
        joined_strings = [file_name,]
        if add_time_slug:
            joined_strings.append(time_slug)
        if add_class_name:
            joined_strings.append(class_name)
        file_root = '_'.join(joined_strings)
        np.save(file_root + "_vocabularray", self.vocabularray)
        np.save(file_root + "_doctionarray", self.doctionarray)
        class_name = self.__class__.__name__
        sp.save_npz(file_root + '.npz', self.bag)
        if self.verbose:
            t0 = dt.now()
            mess = "{}, Save ".format(t0.strftime("%Y-%m-%d %H:%M:%S"))
            mess += class_name+" OK"
            print(mess)
        return self
    