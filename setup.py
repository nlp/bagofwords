#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open('requirements.txt', 'r') as file:
    requirements_ = [line.strip() for line in file.readlines()
                     if not line.startswith('#')]


setuptools.setup(
    name="iambagging", # Replace with your own username
    version="0.2.1",
    author="IAM-CHU-Bordeaux-France",
    author_email="no_email@please.org",
    description="Bag Of Words and its generalization, with Graph tools incorporated. For Natural Language Processing and Information Retrieval",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://eds.chu-bordeaux.fr/gitlab/taliam/bagofwords.git",
    packages=setuptools.find_packages(),
    install_requires = requirements_,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Topic :: Scientific/Engineering :: Information Analysis",
        "Topic :: Text Processing",
        "Intended Audience :: Information Technology",
        "Intended Audience :: Science/Research",
    ],
    python_requires='>=3.6',
)


