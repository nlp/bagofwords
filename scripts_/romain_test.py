#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Création d'un arbre de tokens à partir d'une liste à plat de terme, 
Tentative de construction d'une terminologie
"""

from datetime import datetime as dt
t0 = dt.now()

from i2b2python.i2b2_packaging import i2b2tools as i2b2

from prosit.fetcher import fetcher_i2b2 as i2b2

dir(i2b2)

request = "SELECT * FROM CRBBBS.SAMPLE_TYPE"

i2b2.getI2B2connection()
fetcher = i2b2.fetch_row_by_row(request)

data = list()
for line in fetcher: data.append(line)

text = [str(d['NAME'])+'\n' for d in data]

document = ''.join(text)
len(document)
# print(document)

# character counting
char_count = dict()
for char in document:
    try:
        char_count[ord(char)] += 1
    except KeyError:
        char_count[ord(char)] = 1

chars = [chr(i) if i in char_count.keys() else chr(32) for i in range(33,127)]
char_string = ''.join(chars)
chars_extra = [chr(i) for i in char_count.keys() if i > 127]

replace_chars = {'é':'e','É':'E', 'è':'e', 'ï':'i', 'ê':'e', 'â':'a'}
replace_punct = {'\(':' ','\)':' ',',':' ','-':' '}

from cleaner import ReversibleCleaner

cleaner = ReversibleCleaner(string=document,reversible=True)

cleaner.replace(replace_chars)
cleaner.replace(replace_punct)
cleaner.lower()

from tokenizer import Token, Tokens
import re 

stopwords = ['sur','de',"d'",'du',"l'","la"]

token = Token(string=cleaner.string)
cuts = [(r.start(),r.end()) for r in re.finditer('\n',str(token))]
tokens = token.split(cuts)
clean_tokens = tokens[::2]
catalog = [str(tok) for tok in clean_tokens if tok]
tokens_catalog = list()
for doc in catalog:
    cuts = [(r.start(),r.end()) for r in re.finditer('\s+',str(doc))]
    if cuts:
        tokens = Token(string=doc).split(cuts)
        tokens = tokens[::2]
    else:
        tokens = Tokens([Token(string=doc),])
    for i,tok in enumerate(tokens):
        if str(tok) in stopwords:
            tok.setattr('stopwords',dict(stopwords=True,position=i,length=len(tokens)))
        else:
            tok.setattr('stopwords',dict(stopwords=False,position=i,length=len(tokens)))
    tokens_catalog.append(tokens)

new_catalog = list()
for tokens in tokens_catalog:
    new_tokens = Tokens()
    pass_next, stopwords_tokens = False, []
    for i,tok in enumerate(tokens):
        if tok.stopwords['stopwords'] and i<len(tokens)-1:
            stopwords_tokens.append(tok)
            pass_next = True
        elif pass_next:
            stopwords_tokens.append(tok)
            new_tokens.append(Tokens(stopwords_tokens).join())
            stopwords_tokens = []
            pass_next = False
        else:
            new_tokens.append(tok)
    new_catalog.append(new_tokens)

new_catalog = [[str(tok) for tok in toks] for toks in new_catalog]
    
# stopwords_attributes = {i:[tok.stopwords['position'] for tok in toks
#                          if tok.stopwords['stopwords']]
#                         for i,toks in enumerate(tokens_catalog) 
#                         if any(tok.stopwords['stopwords'] for tok in toks)}
# stopwords_slices = dict()
# for ind,inds in stopwords_attributes.items():
#     stopwords_slices[ind] = [slice(inds[0],inds[0]+2),]
#     for i in inds[1:]:
#         if i<stopwords_slices[ind][-1].stop:
#             stopwords_slices[ind][-1] = slice(stopwords_slices[ind][-1].start,
#                                               stopwords_slices[ind][-1].stop+1)
#         else:
#             stopwords_slices[ind].append(slice(i,i+2))


from bagofwords import NextOfWords
import numpy as np

now = NextOfWords()

NOW = now.fit_transform(new_catalog)
adj_list = now.adjacency_list(tokens=True)
adj_list['biopsie']

slices = now._get_doc_slices() # reconstruct document by document
adj_lists = list()
for sl in slices:
    array = NOW[sl,:].todense()
    nnow = NextOfWords(bow=array,vocabularray=now.vocabularray_)
    al = {str(nnow.vocabularray_[nnow.bow_[0,0]-1]):[]}
    adj_l = nnow.adjacency_list(tokens=True)
    al.update({str(k):list(str(s) for s in v.keys()) for k,v in adj_l.items() if v})
    adj_lists.append(al)

t0 = dt.now() - t0
print("elapsed time : {:.4} s".format(t0.total_seconds()))

#%% vérification
tokens = cleaner.string.split()
s = 'substance'
nexts = set([tok2 for tok1,tok2 in zip(tokens[:-1],tokens[1:]) if tok1==s])
verif = set(c for c in adj_list[s].keys())
verif == nexts

#%% 
all_parents = {s:[] for se in adj_lists for s in se.keys()}
len(all_parents)
for parent,sons in all_parents.items():
    for adj_dict in adj_lists:
        if parent in adj_dict.keys():
            if adj_dict[parent]:
                new_line = ' '.join([parent,]+adj_dict[parent])
                sons.append(new_line)


test_list = [t for t in catalog if t.startswith('transverse')]

all_parents['inconnu']

#%% redo everything in a simpler form

catalog = [c.split() for c in cleaner.string.split('\n') if c]
new_catalog = list()
for doc in catalog:
    new_tokens = list()
    pass_next, stopwords_tokens = False, []
    for i,tok in enumerate(doc):
        if tok in stopwords and i<len(tokens)-1:
            stopwords_tokens.append(tok)
            pass_next = True
        elif pass_next:
            stopwords_tokens.append(tok)
            new_tokens.append(' '.join(stopwords_tokens))
            stopwords_tokens = []
            pass_next = False
        else:
            new_tokens.append(tok)
    new_catalog.append(new_tokens)

all_parents = {s[0]:[] for s in new_catalog}
len(all_parents)
